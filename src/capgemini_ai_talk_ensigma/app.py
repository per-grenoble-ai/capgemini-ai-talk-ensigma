"""The Rest API application entrypoint."""
import asyncio
import concurrent.futures
import time
from random import randint

from fastapi import FastAPI

app = FastAPI(
    title="Demonstration",
    description="A minimal REST API built using FastAPI framework.",
)


pool = concurrent.futures.ThreadPoolExecutor(max_workers=10)
loop = asyncio.get_running_loop()


def blocking_process(execution_time):
    time.sleep(execution_time)
    return {"label": 0, "probabilities": {0: 0.25, 1: 0.75}}


@app.get("/")
async def get_root():
    # Let's say that our function should take between 5ms and 30ms to run
    execution_time = randint(5, 30) / 1000
    # We simulate the compute time
    await loop.run_in_executor(pool, blocking_process, execution_time)
    # And we return a value
    return {"label": 0, "probabilities": {0: 0.25, 1: 0.75}}


@app.on_event("shutdown")
def close_pool():
    pool.close()
