"""Command Line Interface to start the REST API application."""
import uvicorn
from typer import Typer

APP = "capgemini_ai_talk_ensigma.app:app"


cli = Typer(name="demo-ensigma")


@cli.command("start")
def start_app(
    host: str = "127.0.0.1",
    port: int = 5000,
    reload: bool = True,
    log_level: str = "debug",
    workers: int = 1,
):
    uvicorn.run(
        APP, host=host, port=port, reload=reload, log_level=log_level, workers=workers
    )
