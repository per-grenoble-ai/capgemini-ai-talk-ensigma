"""Dummy tests to ensure pipeline does not fail until tests are implemented.

By default, pytest will fail if no tests are discovered.
"""


def test_dummy():
    """A dummy test that will always succeed."""
    assert True
